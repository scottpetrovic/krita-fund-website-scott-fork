#!/bin/bash

set -e

BACKUP_DIR={{ backup_dir }}
BACKUP_RETENTION={{ retention }}
BACKUP_EXT=tar.xz
SUFFIX=`date '+%Y-%m-%d-%s'`

rotate_backups () {
    set +e
    cd $BACKUP_DIR
    # echo "Rotating $1 .. $BACKUP_EXT in $BACKUP_DIR"
    ls -t | grep $1 | grep $BACKUP_EXT | sed -e 1,"$BACKUP_RETENTION"d | xargs -d '\n' rm -R > /dev/null 2>&1
    set -e
}

for d in $@
do
    parent_dir=`dirname $d`
    name=`basename $d`
    cd $parent_dir
    prefix=`echo $d | sed 's/\//-/g' | sed 's/^-//'`
    filename=$BACKUP_DIR/$prefix-$SUFFIX.$BACKUP_EXT
    # echo "Archiving $name from $parent_dir to $filename"
    tar cf - $name | xz -z - > $filename
    rotate_backups $prefix
done
