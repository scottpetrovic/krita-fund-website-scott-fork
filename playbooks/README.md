# Deployment playbooks

This deployment setup is currently a work in progress.

The target system is assumed to have Ubuntu 20.04 (focal) installed,
for other distros or releases the playbooks have not been tested,
and will most likely fail due to differences in configuration paths and so on.

To avoid adding more dependencies to DevFund itself, `ansible` uses its own `virtualenv`.
To set it up use the following commands:

    virtualenv .venv -p python
    source .venv/bin/activate
    pip install -r requirements.txt

At the moment there's only a production installation of DevFund.
First time installation playbook `install.yaml` does not setup production-ready `settings.py`.
These must be created after at the following path:

    /opt/blender-fund-production/blender_fund/settings.py

Encrypt any sensitive values with Ansible Vault using `production` vault ID and passwords.
Any playbooks that use these values will need to be able to decrypt them,
so use `--vault-id production@prompt`: this will make Ansible prompt for a Vault password.

## First time install

    ./ansible.sh -i environments/production install.yaml --vault-id production@prompt
    ./ansible.sh -i environments/production setup_certificate.yaml

## Deploy

Except for error page templates, which are part of the playbooks,
the playbooks do not deploy local uncommitted changes.
When you need to deploy something, make sure to commit and push your changes both to
`master` and `production`:

1. commit and push your changes to `master`;
2. push the same exact changes to `production` using the following:

    git checkout production
    git pull
    git merge --ff-only master
    git push

3. navigate to the playbooks and run `deploy.yaml`

    ./ansible.sh -i environments/production deploy.yaml

### Periodic tasks

DevFund is using `crontab` for periodic tasks such as cleaning up old sessions
and processing subscription payments.

To install or update these, use the following playbook:

    ./ansible.sh -i environments/production configure_crontab.yaml

To see current state of production `crontab`, use the following commands
at the target server:

    sudo -Hu blender-fund-production crontab -l
