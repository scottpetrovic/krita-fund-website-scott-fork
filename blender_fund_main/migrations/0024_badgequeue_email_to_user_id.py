# Generated by Django 2.1.3 on 2018-12-18 16:45

from django.conf import settings
from django.db import migrations
from django.db.transaction import atomic


@atomic
def email_to_uid(apps, schema_editor):
    BadgerQueuedCall = apps.get_model('blender_fund_main', 'BadgerQueuedCall')
    User = apps.get_model('auth', 'User')

    for call in BadgerQueuedCall.objects.all():
        try:
            user = User.objects.get(email=call.email)
        except User.DoesNotExist:
            raise RuntimeError(f'user with email {call.email!r} does not exist!')
        call.user = user
        call.save()


@atomic
def uid_to_email(apps, schema_editor):
    BadgerQueuedCall = apps.get_model('blender_fund_main', 'BadgerQueuedCall')

    for call in BadgerQueuedCall.objects.all():
        call.email = call.user.email
        call.save()


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blender_fund_main', '0023_badgequeue_email_to_user_id'),
    ]

    operations = [
        migrations.RunPython(email_to_uid, uid_to_email),
    ]
