from django.views.generic import ListView

from blender_fund_main.models import Activity


class ActivityListView(ListView):
    paginate_by = 25
    model = Activity
