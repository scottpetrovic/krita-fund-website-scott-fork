import typing
from unittest import mock

import django.core.mail
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.test import override_settings
from django.urls import reverse

from blender_fund_main.email import absolute_url
from looper.models import Subscription
import looper.tests.test_checkout
from looper.tests import AbstractBaseTestCase
from ..models import Membership


# Prevent communication with Google's reCAPTCHA API.
@override_settings(GOOGLE_RECAPTCHA_SECRET_KEY='')
class CheckoutTestCase(AbstractBaseTestCase):
    fixtures = ['devfund', 'systemuser']

    def setUp(self):
        self.user = User.objects.create_user('explodingchicken', 'chicken@example.com')

    @mock.patch('looper.gateways.BraintreeGateway.transact_sale')
    def test_checkout_create_valid_membership(self, mock_transact_sale):
        from looper.signals import subscription_activated
        from ..signals import membership_activated

        created_subs: typing.Optional[Subscription] = None
        created_memb: typing.Optional[Membership] = None

        @receiver(subscription_activated)
        def subs_activated(sender: Subscription, **kwargs):
            nonlocal created_subs
            created_subs = sender

        @receiver(membership_activated)
        def memb_activated(sender: Membership, **kwargs):
            nonlocal created_memb
            created_memb = sender

        mock_transact_sale.return_value = 'mock-transaction-id'

        self.client.force_login(self.user)
        payload = {
            'gateway': 'braintree',
            'payment_method_nonce': 'fake-valid-nonce',
            'full_name': 'Erik von Namenstein',
            'company': 'Nöming Thingéys',
            'street_address': 'Scotland Pl',
            'locality': 'Amsterdongel',
            'postal_code': '1025 ET',
            'region': 'Worbelmoster',
            'country': 'NL',
        }
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode())

        self.assertIsNotNone(created_subs)
        self.assertEqual('active', created_subs.status)
        self.assertEqual('mock-transaction-id',
                         created_subs.latest_order().latest_transaction().transaction_id)

        membership = Membership.objects.get(subscription=created_subs)
        self.assertEqual('active', membership.status)
        self.assertEqual(membership, created_memb)

    def test_bank(self):
        from looper.signals import subscription_activated
        from ..signals import membership_activated, membership_created_needs_payment

        sig_receiver = mock.Mock()
        subscription_activated.connect(sig_receiver)
        membership_activated.connect(sig_receiver)
        membership_created_needs_payment.connect(sig_receiver)

        self.client.force_login(self.user)
        payload = {
            'gateway': 'bank',
            'payment_method_nonce': 'fake-valid-nonce',
            'full_name': 'Erik von Namenstein',
            'company': 'Nöming Thingéys',
            'street_address': 'Scotland Pl',
            'locality': 'Amsterdongel',
            'postal_code': '1025 ET',
            'region': 'Worbelmoster',
            'country': 'NL',
        }
        url = reverse('looper:checkout', kwargs={'plan_id': 2, 'plan_variation_id': 5})
        r = self.client.post(url, data=payload)
        self.assertEqual(302, r.status_code, r.content.decode())

        created_subs = Subscription.objects.first()
        sig_receiver.assert_called_once_with(sender=created_subs.membership,
                                             signal=membership_created_needs_payment)

        self.assertIsNotNone(created_subs)
        self.assertEqual('on-hold', created_subs.status)

        self.assertIsNotNone(created_subs.membership)
        self.assertEqual('inactive', created_subs.membership.status)

        # An email should have been sent with the bank info.
        self.assertEqual(1, len(django.core.mail.outbox))
        the_mail: django.core.mail.message.EmailMultiAlternatives = django.core.mail.outbox[0]
        self.assertIn(created_subs.membership.level.name, the_mail.body)
        self.assertIn(created_subs.price.with_currency_symbol(), the_mail.body)

        edit_url = absolute_url('settings_membership_edit',
                                kwargs={'membership_id': created_subs.membership.id})
        self.assertIn(edit_url, the_mail.body)
        self.assertIn(created_subs.membership.level.name, the_mail.body)
        alt0_body, alt0_type = the_mail.alternatives[0]
        self.assertEqual(alt0_type, 'text/html')


class MockedCheckoutTest(looper.tests.test_checkout.MockedCheckoutTest):
    def test_processor_declined(self):
        self.assertEqual(0, Membership.objects.count())

        super().test_processor_declined()

        # After the fail-and-then-succeed of the superclass' test, we should have
        # only one membership, and it should be active.
        self.assertEqual(1, Membership.objects.count())

        memb = Membership.objects.first()
        self.assertEqual('active', memb.status)
