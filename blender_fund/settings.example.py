import os

import braintree

# noinspection PyUnresolvedReferences
from .settings_common import *

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'

DEBUG = True
SECRET_KEY = 'oq)kz$ap5xh(2&dqnzs!f9_2#xa9u@08$4x@xu7k$mq5v7z3cr'

BLENDER_ID = {
    # Configure the OAuth Callback URL at Blender ID to something like:
    # http://fund.local:8010/oauth/authorized
    'BASE_URL': 'http://id.local:8000/',
    'OAUTH_CLIENT': '--set-in-settings.py--',
    'OAUTH_SECRET': '--set-in-settings.py--',
    'BADGER_API_SECRET': '--set-in-settings.py--',
}

GATEWAYS = {
    'braintree': {
        'environment': braintree.Environment.Sandbox,
        'merchant_id': 'SECRET',
        'public_key': 'SECRET',
        'private_key': 'SECRET',

        # Merchant Account IDs for different currencies.
        # Configured in Braintree: Account → Merchant Account Info.
        'merchant_account_ids': {
            'EUR': 'merchant-account-id-for-eur',
            'USD': 'merchant-account-id-for-usd',
        },
        # DevFund allows only automatic collection with Braintree:
        'supported_collection_methods': {'automatic'},
    },
    'bank': {
        'supported_collection_methods': {'manual'},
    },
}

# HTTP clients on those IP addresses get to see the Django Debug Toolbar.
INTERNAL_IPS = ['127.0.0.1']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'default': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(message)s'
        },
        'verbose': {
            'format': '%(asctime)-15s %(levelname)8s %(name)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'default',  # Set to 'verbose' in production
            'stream': 'ext://sys.stderr',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        'blender_fund': {'level': 'DEBUG'},
        'blender_fund_main': {'level': 'DEBUG'},
        'blender_notes': {'level': 'DEBUG'},
        'looper': {'level': 'DEBUG'},
        'blender_id_oauth_client': {'level': 'DEBUG'},
    },
    'root': {
        'level': 'WARNING',
        'handlers': [
            'console',
            'mail_admins',
        ],
    }
}

# For development, dump email to the console instead of trying to actually send it.
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# Alternatively, run python3 -m smtpd -n -c DebuggingServer -d '0.0.0.0:2525' and set
# EMAIL_PORT = 2525

# Used by Django to send error mails.
ADMINS = [
    ('Your Name', 'you+fund@blender.studio'),
]

# For collecting usage metrics
GOOGLE_ANALYTICS_TRACKING_ID = ''

# For enabling reCaptcha v2:
GOOGLE_RECAPTCHA_SECRET_KEY = ''
GOOGLE_RECAPTCHA_SITE_KEY = ''


# Uncomment to enable Sentry in production.
#
# The send_default_pii parameter controls whether it sends personally
# identifyable information (such as user ids, usernames, cookies,
# authorization headers, ip addresses) to Sentry.
# import sys
#
# if sys.stderr.isatty():
#     print('Skipping Sentry initialisation because stderr is a TTY', file=sys.stderr)
# else:
#     import logging
#     import sentry_sdk
#     from sentry_sdk.integrations.django import DjangoIntegration
#     from sentry_sdk.integrations.logging import LoggingIntegration
#
#     # By default Sentry only sends ERROR level and above.
#     sentry_logging = LoggingIntegration(
#         level=logging.INFO,          # Capture this level and above as breadcrumbs
#         event_level=logging.WARNING  # Send this level and above as events
#     )
#     sentry_sdk.init(
#         dsn="https://xxxxxxxxxxxx@sentry.io/yyyyyyyyy",
#         send_default_pii=False,
#         integrations=[sentry_logging, DjangoIntegration()]
#     )
